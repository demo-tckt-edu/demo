-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2018 at 05:08 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kpis_e-cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `k_post`
--

CREATE TABLE `k_post` (
  `id` int(10) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` int(11) DEFAULT NULL,
  `link` text COLLATE utf8mb4_unicode_ci,
  `img` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `tag` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `k_post`
--

INSERT INTO `k_post` (`id`, `name`, `author`, `date`, `categories`, `link`, `img`, `content`, `short_description`, `tag`, `type`) VALUES
(1, 'Các điểm nổi bật trên tủ lạnh Samsung 2018', 'nam', '30/9/2018', 1, 'http://demo03.kpis.vn/admin/PostList.php?route=journal3/journal3&user_token=R8VYsfSXJ3773Ay0X1ol2Ix3aSeBGm08#/blog_post/edit/1', '‰PNG\r\n\Z\n', 'http://demo03.kpis.vn/admin/PostList.php?route=journal3/journal3&user_token=R8VYsfSXJ3773Ay0X1ol2Ix3aSeBGm08#/blog_post/edit/1http://demo03.kpis.vn/admin/PostList.php?route=journal3/journal3&user_token=R8VYsfSXJ3773Ay0X1ol2Ix3aSeBGm08#/blog_post/edit/1\r\nhttp://demo03.kpis.vn/admin/PostList.php?route=journal3/journal3&user_token=R8VYsfSXJ3773Ay0X1ol2Ix3aSeBGm08#/blog_post/edit/1\r\nhttp://demo03.kpis.vn/admin/PostList.php?route=journal3/journal3&user_token=R8VYsfSXJ3773Ay0X1ol2Ix3aSeBGm08#/blog_post/edit/1\r\nhttp://demo03.kpis.vn/admin/PostList.php?route=journal3/journal3&user_token=R8VYsfSXJ3773Ay0X1ol2Ix3aSeBGm08#/blog_post/edit/1', 'http://demo03.kpis.vn/admin/PostList.php?route=journal3/journal3&user_token=R8VYsfSXJ3773Ay0X1ol2Ix3aSeBGm08#/blog_post/edit/1', 'âsfasfa', 'tin tức'),
(2, 'Loa LG thương hiệu của nước nào? Có tốt không?', 'nam', '30/9/2018', 2, 'http://demo03.kpis.vn/admin/PostList.php?route=journal3/journal3&user_token=R8VYsfSXJ3773Ay0X1ol2Ix3aSeBGm08#/blog_post/edit/2', 'ÿØÿá', 'http://demo03.kpis.vn/admin/PostList.php?route=journal3/journal3&user_token=R8VYsfSXJ3773Ay0X1ol2Ix3aSeBGm08#/blog_post/edit/2\r\nhttp://demo03.kpis.vn/admin/PostList.php?route=journal3/journal3&user_token=R8VYsfSXJ3773Ay0X1ol2Ix3aSeBGm08#/blog_post/edit/2\r\nhttp://demo03.kpis.vn/admin/PostList.php?route=journal3/journal3&user_token=R8VYsfSXJ3773Ay0X1ol2Ix3aSeBGm08#/blog_post/edit/2\r\n', 'http://demo03.kpis.vn/admin/PostList.php?route=journal3/journal3&user_token=R8VYsfSXJ3773Ay0X1ol2Ix3aSeBGm08#/blog_post/edit/2', 'tisna-ti', 'tin tức');

-- --------------------------------------------------------

--
-- Table structure for table `k_user`
--

CREATE TABLE `k_user` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resertcode` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `k_post`
--
ALTER TABLE `k_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `k_user`
--
ALTER TABLE `k_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `k_post`
--
ALTER TABLE `k_post`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `k_user`
--
ALTER TABLE `k_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `k_user`
--
ALTER TABLE `k_user`
  ADD CONSTRAINT `k_user_ibfk_1` FOREIGN KEY (`id`) REFERENCES `k_post` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
