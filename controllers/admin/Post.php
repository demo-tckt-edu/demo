<?php

class Post extends Controller {

	function __construct() {
		parent::__construct();

	}
    function PostList() {
        $this->view->title = 'List Post';
        if (isset($this->model)) {
            $this->view->PostListModel = $this->model->PostListModel();
        }
        $this->view->render('admin/Post/PostList');
    }

    function CategoryPost() {
        $this->view->title = 'Category Post';
        $this->view->render('admin/Post/CategoryPost');
    }
    function AddPost() {
        $this->view->title = 'Add Post';
        $this->view->render('admin/Post/AddPost');
    }
    function TagPost() {
        $this->view->title = 'Tag';
        $this->view->render('admin/Post/TagPost');
    }
}