<?php

class User extends Controller {

	function __construct() {
		parent::__construct();

	}	
	function UserList() {
		$this->view->title = 'List';
        $this->view->UserListModel = $this->model->UserListModel();
        $this->view->render('admin/User/UserList');
	}
	 
    function DetailUser() {
        $this->view->title = 'Detail';
        $this->view->render('admin/User/DetailUser');
    }
    function AddUser() {
        $this->view->title = 'Add User';
        $this->view->render('admin/User/AddUser');
    }
}