<?php

class Index extends Controller {

	function __construct() {
		parent::__construct();
	}
	
	function index() {
		// this->view-t
		$this->view->render('home/home');
	}
	
	function details() {
		$this->view->render('home/details');
	}
	
}