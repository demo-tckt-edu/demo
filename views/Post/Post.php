  <?php   
 include'views/layout/head.php';
  include'views/layout/header.php';
?>                         
                            <div class="block-nopadding no-padding-home">
                                <div class="content noidung">
                                    <div class="col-sm-9 no-padding-left padding-right-5">
                                                                                                                                  <!-- Template Start: site/news/WrapDetails -->
<div class="detail-news">
    
    <div class="content-block">
        <div class="col-sm-12 no-padding-left padding-bottom">
            <span class="title-block text-blue nopadding">Thông báo tuyển sinh ĐH chính quy Văn bằng thứ 2 năm 2018 – Đợt 2</span>
        </div>
        <div class="col-sm-12 nopadding news-info">
            <div class="col-sm-6 nopadding">
                <div class="article-tools clearfix">
                    <dl style="width: auto;" class="article-info">
                        <dd class="create"><i class="fa fa-calendar" aria-hidden="true"></i> <span>30/07/2018</span></dd>
                    </dl>
                </div>
            </div>
            <div class="col-sm-6 nopadding text-right">
                <div id="fb-root"></div>
                <script>(function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id))
                            return;
                        js = d.createElement(s);
                        js.id = id;
                        js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11&appId=528178050544439';
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                <div class="share">
                    <span class="cmp_like_container"></span>
                    <span class="cmp_share_container"></span>
                </div>
            </div>
        </div>
        <panel id="contentnews">
            <p style="text-align: justify;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">Hội đồng tuyển sinh Trường Đại học Tài Chính Kế Toán thông báo tuyển sinh Đại học hệ chính quy văn bằng thứ 2 năm 2018&nbsp;– Đợt 2, cụ thể ngành đào tạo như sau:</span></span><br>
&nbsp;</p>

<p style="text-align: justify;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><span style="color:rgb(255, 0, 0)"><span style="color:rgb(0, 0, 255)"><strong>I. </strong></span><strong><span style="color:rgb(0, 0, 255)">ĐỐI TƯỢNG TUYỂN SINH</span></strong></span></span></span></p>

<p style="text-align: justify;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">Những thí sinh đang làm việc trong các cơ quan, công ty, các tổ chức kinh tế đã tốt nghiệp một bằng đại học không phân biệt loại hình đào tạo.</span></span><br>
&nbsp;</p>

<p style="text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><img alt="" src="<?php echo ROOT_URL; ?>/public/images/post.jpg" style="height:413px; width:620px"><br>
<span style="color:#808080"><em>Trường Đại Học Tài Chính - Kế Toán tuyển sinh Đại học chính quy VB2 với những thí sinh đã tốt nghiệp một bằng đại học</em></span></span></span></p>

<p style="text-align: justify;"><br>
<span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><span style="color:rgb(255, 0, 0)"><span style="color:rgb(0, 0, 255)"><strong>II. </strong></span><strong><span style="color:rgb(0, 0, 255)">CHUYÊN NGÀNH ĐÀO TẠO VÀ HÌNH THỨC ĐÀO TẠO</span></strong></span></span></span><br>
&nbsp;</p>

<table align="center" border="0" class="has-border" style="height:274px; width:657px">
    <tbody>
        <tr>
            <td colspan="1" rowspan="2" style="background-color: #3399ff; text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><span style="color:rgb(255, 255, 255)"><strong>TT</strong></span></span></span></td>
            <td colspan="1" rowspan="2" style="background-color: #3399ff; text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><span style="color:rgb(255, 255, 255)"><strong>NGÀNH ĐÀO TẠO</strong></span></span></span></td>
            <td colspan="1" rowspan="2" style="background-color: #3399ff; text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><span style="color:rgb(255, 255, 255)"><strong>MÃ NGÀNH</strong></span></span></span></td>
            <td colspan="2" rowspan="1" style="background-color: #3399ff; text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><span style="color:rgb(255, 255, 255)"><strong>MÔN THI TUYỂN</strong></span></span></span></td>
        </tr>
        <tr>
            <td style="text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><strong>MÔN 1</strong></span></span></td>
            <td style="text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><strong>MÔN 2</strong></span></span></td>
        </tr>
        <tr>
            <td style="text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><strong>1</strong></span></span></td>
            <td><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><strong>Kỹ thuật xây dựng</strong></span></span></td>
            <td style="text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><strong>7580201</strong></span></span></td>
            <td style="text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">Tiếng Anh</span></span></td>
            <td style="text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">Tin học cơ bản</span></span></td>
        </tr>
        <tr>
            <td style="text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><strong>2</strong></span></span></td>
            <td><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><strong>Quản trị kinh doanh</strong></span></span></td>
            <td style="text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><strong>7340101</strong></span></span></td>
            <td style="text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">Tiếng Anh</span></span></td>
            <td style="text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">Tin học cơ bản</span></span></td>
        </tr>
        <tr>
            <td style="text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><strong>3</strong></span></span></td>
            <td><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><strong>Ngôn ngữ Anh</strong></span></span></td>
            <td style="text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><strong>7220201</strong></span></span></td>
            <td style="text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">Tiếng Anh</span></span></td>
            <td style="text-align: center;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">Tin học cơ bản</span></span></td>
        </tr>
    </tbody>
</table>

<p style="text-align: justify; padding-left: 30px;"><br>
<span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">Đào tạo theo học chế tín chỉ, thời gian đào tạo từ 2 - 2,5 năm (tùy theo ngành đào tạo). Sinh viên tốt nghiệp sẽ được cấp bằng đại học hệ chính quy văn bằng thứ 2 trong hệ thống văn bằng quốc gia, học vị <strong><em>Kỹ sư hoặc Cử nhân</em></strong>.</span></span><br>
&nbsp;</p>
<span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><strong><span style="color:rgb(255, 0, 0)"><span style="color:rgb(0, 0, 255)">III. HỒ SƠ DỰ THI</span></span></strong></span></span>

<ol>
    <li><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">01 bộ hồ sơ dự thi tuyển <em>(theo mẫu<strong>)</strong></em></span></span></li>
    <li><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">02 bản sao hợp lệ Bằng tốt nghiệp Đại học thứ nhất.</span></span></li>
    <li><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">02 bản sao hợp lệ Bảng điểm toàn khóa học Đại học thứ nhất.</span></span></li>
    <li><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">Bản sao hợp lệ bằng tốt nghiệp Trung học phổ thông.</span></span></li>
    <li><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">Bản sao hợp lệ Giấy khai sinh.</span></span></li>
    <li><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">Các giấy chứng nhận ưu tiên (nếu có), bản chính hoặc bản sao hợp lệ.</span></span></li>
    <li><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">Giấy chứng nhận sức khỏe do cơ sở y tế Quận, Huyện <em>(hoặc cấp thẩm quyền tương đương theo quy định hiện hành của các cơ quan chức năng nhà nước)</em> cấp.</span></span></li>
    <li><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">02 ảnh màu mới chụp (cỡ 3x4) chưa quá 6 tháng, phía sau ghi rõ họ tên, ngày tháng năm sinh.</span></span></li>
    <li><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">Bản photo CMND</span></span></li>
</ol>
<span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">Thí sinh mua hồ sơ tại <strong>Phòng Tư vấn - Tuyển sinh - Truyền thông</strong> hoặc tải xuống trong file đính kèm; nộp hồ sơ trực tiếp tại&nbsp;<strong>Phòng tư vấn - Tuyển sinh - Truyền thông</strong> kể từ ngày ra thông báo đến hết&nbsp;ngày<span style="color:#FF0000"><strong>&nbsp;22/10/2018</strong></span>.<br>
<strong>- Lệ phí hồ sơ</strong>:&nbsp; 30.000 đồng /1 hồ sơ.<br>
- <strong>Lệ phí dự thi</strong>: 300.000 đồng /1 thí sinh (nộp khi đến nộp hồ sơ).</span></span><br>
<br>
<span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><span style="color:rgb(0, 0, 255)"><strong>IV. THỜI GIAN NHẬN HỒ SƠ:<br>
&nbsp; - Sáng:&nbsp; <span style="color:rgb(255, 0, 0)">từ 7h30 đến 11h30</span> </strong></span>(từ thứ Hai đến thứ Bảy)<br>
<span style="color:rgb(0, 0, 255)"><strong>&nbsp; - Chiều: <span style="color:rgb(255, 0, 0)">từ 13h30 đến 16h30</span> </strong></span>(từ thứ Hai đến thứ Sáu)<br>
<span style="color:rgb(0, 0, 255)"><strong>&nbsp; - Tối :&nbsp;&nbsp;&nbsp; <span style="color:rgb(255, 0, 0)">từ 17h00 đến 19h30</span> </strong></span>(từ thứ Hai đến thứ Sáu)</span></span>

<p><br>
<span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><span style="color:rgb(0, 0, 255)"><strong>V. THI TUYỂN</strong></span></span></span></p>

<p style="padding-left: 30px;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><strong>- Ngày thi</strong>: <span style="color:#FF0000"><strong>11/11/2018</strong></span>.<br>
- Thí sinh nhận "Giấy báo dự thi" tại trường từ <span style="color:#FF0000"><strong>02/11/2018</strong></span><br>
- Trường tổ chức ôn thi cho các thí sinh có yêu cầu. Thời gian ôn thi từ <span style="color:#FF0000"><strong>01/10/2018</strong></span><span style="color:#0000FF"><strong>&nbsp;</strong></span></span></span><br>
<span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif">- Lệ phí ôn thi: <strong>300.000 đồng / 1 môn. </strong><em>(Sinh viên Đại Học Tài Chính - Kế Toán sẽ được giảm 50% lệ phí ôn thi)</em></span></span></p>

<p style="padding-left: 30px;"><br>
<span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><em><strong>Thông tin liên hệ:<br>
Trường Đại học Tài Chính - Kế Toán)<br>
Phòng Tư vấn - Tuyển sinh - Truyền thông<br>
Thị Trấn La Hà - Tư Nghĩa - Quảng Ngãi<br>
Điện thoại: 0255. 3 845 578- Email: info@tckt.edu.vn</strong></em></span></span><br>
&nbsp;</p>

<p style="padding-left: 30px;"><span style="font-size:13px"><span style="font-family:arial,helvetica,sans-serif"><span style="color:rgb(0, 0, 255)"><strong>Phòng Tư vấn - Tuyển sinh - Truyền thông</strong></span></span></span></p>
                        <div class="attachments">
                <div class="attachcontent">
                <strong>Đính kèm:</strong>
                                    <div class="filename">

                    <a href="#">LYLICH_SINH_VIEN.doc</a>
                    </div>
                                    <div class="filename">

                    <a href='#'>PHIEUDANG-KY ON THI.doc</a>
                    </div>
                                    <div class="filename">

                    <a href='#'>thong_bao_tuyen_sinh_van_bang 2 dot_2.pdf</a>
                    </div>
                                </div>
            </div>
                        <div class="idshow">
            <panel id="idnews"></panel>
                
                12170 
                        <panel id="endidnews"></panel>
            </div>
        </panel>
        <panel id="endNews"></panel>
                
        <hr />   
        <div class="extranews_box">
            <h2><strong>Các tin khác</strong></h2>
            <div class="extranews_older">
                <ul class="older">
                                                                                                            
                    <li class="wow slideInUp animated">
                        <a href='#'>Thông báo tuyển sinh ĐH chính quy Văn bằng thứ 2 năm 2018 – Đợt 2</a>
                    </li>
                                                                                                            
                    <li class="wow slideInUp animated">
                        <a href='#'>Thông báo tuyển sinh liên thông TCCN, Cao đẳng, Cao đẳng nghề lên Đại học đợt 2 năm 2018</a>
                    </li>
                                                                                                            
                    <li class="wow slideInUp animated">
                        <a href='#'>Tuyển sinh đào tạo trình độ Thạc sĩ Đợt 2 – năm 2018</a>
                    </li>
                                                                                                            
                    <li class="wow slideInUp animated">
                        <a href='#'>Tuyển sinh đào tạo trình độ Tiến sĩ Đợt 2 - năm 2018</a>
                    </li>
                                                                                                            
                    <li class="wow slideInUp animated">
                        <a href='#'>Thông báo tuyển sinh Đào tạo từ xa trình độ Đại học - Khóa 2018</a>
                    </li>
                                                                                                            
                    <li class="wow slideInUp animated">
                        <a href='#'>Thông báo tuyển sinh Cao đẳng thực hành năm 2016</a>
                    </li>
                                    </ul>
            </div>
        </div>
            </div>
</div>

 
<!-- Template End: site/news/WrapDetails -->
                                                    
                                         
                                    </div>
                                    <div class="col-sm-3 nopadding">
                                         
                                                                                            <!-- Template Start: site/common/WrapNotify -->
    <div class="block-content">
        <div class="header-block">
            <img class="icon-tin-noi-bat" src="//www.hutech.edu.vn/images/img_blank.gif" />
            <span class="title-block text-red">
                                    <div class="title-right"> Thông tin mới</div>
                 
            </span>
        </div>
        <div class="content-block">
            <ul class="latestnews">
                                                                                    <li class="wow slideInUp animated">
                    <a class="latestnews" href="#">
                       Thông báo tuyển sinh ĐH chính quy Văn bằng thứ 2 năm 2018 – Đợt 2                    </a>
                </li>
                                                                                    <li class="wow slideInUp animated">
                    <a class="latestnews" href="#">
                       Thông báo tuyển sinh liên thông TCCN, Cao đẳng, Cao đẳng nghề lên Đại học đợt 2 năm 2018                    </a>
                </li>
                                                                                    <li class="wow slideInUp animated">
                    <a class="latestnews" href="#">
                       Tuyển sinh đào tạo trình độ Thạc sĩ Đợt 2 – năm 2018                    </a>
                </li>
                                                                                    <li class="wow slideInUp animated">
                    <a class="latestnews" href="#">
                       Tuyển sinh đào tạo trình độ Tiến sĩ Đợt 2 - năm 2018                    </a>
                </li>
                                                                                    <li class="wow slideInUp animated">
                    <a class="latestnews" href="#">
                       Thông báo tuyển sinh Đào tạo từ xa trình độ Đại học - Khóa 2018                    </a>
                </li>
                                                                                    <li class="wow slideInUp animated">
                    <a class="latestnews" href="#">
                       Mẫu hồ sơ xét tuyển kết quả học tập năm lớp 12 vào ĐH chính quy                    </a>
                </li>
                            </ul>
            <div class="col-sm-12 text-right padding wow slideInUp animated">
                                    <a href="#" class="text-red text-bold"><i class="fa fa-plus-square fa-1 text-red" aria-hidden="true"></i> Xem tất cả</a>
                 
                
                         
            </div>
        </div>
    </div>




<!-- Template End: site/common/WrapNotify -->
                                                                                            
                                                                                            <!-- Template Start: site/tuyensinh/WrapQuickReference -->
<div class="block-content">
    <div id="tsmaudo">
    <a href="#">
    <span class="top" style="display:inline-block;">&nbsp;&nbsp;Tuyển sinh Đại học 2018</span>
    </a>
    </div>
    
    <div id="tsmaucam">
    <a href="#">
    <span class="top" style="display:inline-block;">&nbsp;&nbsp;Tuyển sinh liên thông Đại học</span>
    </a>
    </div>

    <div id="tsxanhlacay">
    <a href="#">
    <span class="top" style="display:inline-block;">&nbsp;&nbsp;Chương trình ĐH chuẩn Nhật bản</span>
    </a>
    </div>

    <div id="tsxanhduong">
    <a href="#">
    <span class="top" style="display:inline-block;">&nbsp;&nbsp;Chương trình bằng Tiếng anh</span>
    </a>
    </div>


    <div id="tsmaudo">
    <a href="#">
    <span class="top" style="display:inline-block;">&nbsp;&nbsp;Chương trình Quốc tế</span>
    </a>
    </div>


    <div id="tsmautim">
    <a href="#">
    <span class="top" style="display:inline-block;">&nbsp;&nbsp;Tuyển sinh Thạc sĩ 2018</span>
    </a>
    </div>

    <div id="tshong">
    <a target="_blank" href="#">
    <span class="top" style="display:inline-block;">&nbsp;&nbsp;Tuyển sinh Tiến sĩ 2018</span>
    </a>
    </div>
    
    <div id="tsxanhla">
    <a target="_blank" href="#">
    <span class="top" style="display:inline-block;">&nbsp;&nbsp;Hồ sơ tuyển sinh ĐH-CĐ 2018</span>
    </a>
    </div>
    
    <div id="tsxanhdam">
    <a target="_blank" href="#">
    <span class="top" style="display:inline-block;">&nbsp;&nbsp;Tư vấn tuyển sinh trực tuyến</span>
    </a>
    </div>

</div>
  

<!-- Template End: site/tuyensinh/WrapQuickReference -->
                                                                                            
                                                                                            
                                                    
                                           
                                    </div>
                                </div>
                            </div>
                                 <?php    require 'views/layout/footer.php';  ?>
                              
                          
                            
  