
                <!-- Template End: site/home/WrapHome -->

                <!-- Template Start: site/common/WrapLienKet -->
               
                <!-- Template End: site/common/WrapLienKet -->

                <!-- Template Start: site/common/WrapFooter -->
                <div class="block-nopadding footer back-blue padding-bottom">
                    <div class="content noidung">
                        <div class="col-sm-9 footer-left">
                            <div><b class="truso"></b>©  Bản quyền Trường Đại học Tài chính - Kế toán</div>
                            <div class="coso"><b>Địa chỉ:</b> Thị Trấn La Hà - Tư Nghĩa - Quảng Ngãi</div>
                            <div>Điện thoại: 0255. 3 845 578- Email:<a href="mailto:info@tckt.edu.vn" style="color:#a2d215"> info@tckt.edu.vn</a></div>
                        </div>
                        <div class="col-sm-3 footer-right">

                            <a href="#">
                                <img class="icon-lien-he-facebook" src="https://www.hutech.edu.vn/images/img_blank.gif" />
                            </a>
                            <a href="#" target="_blank">
                                <img class="icon-lien-he-youtube" src="https://www.hutech.edu.vn/images/img_blank.gif" />
                            </a>
                            <a href="#" target="_blank">
                                <img class="icon-lien-he-google" src="https://www.hutech.edu.vn/images/img_blank.gif" />
                            </a>
                            <a href="#" target="_blank">
                                <img class="icon-lien-he-twice" src="https://www.hutech.edu.vn/images/img_blank.gif" />
                            </a>
                            <a href="#" target="_blank">
                                <img class="icon-lien-he-blog" src="https://www.hutech.edu.vn/images/img_blank.gif" />
                            </a>
                            <a href="#" target="_blank">
                                <img class="icon-lien-he-tum" src="https://www.hutech.edu.vn/images/img_blank.gif" />
                            </a>
                            <br> Bản quyền <span style="font-size: small;"><a href="#"><strong>&copy;</strong></a></span> 2018 
                            <a rel="author" href="#">ĐHTCKT</a>
                        </div>
                    </div>
                </div>
                <!-- Template End: site/common/WrapFooter -->
                <!-- Template Start: site/common/WrapOther -->
                <!-- subiz -->

                <!-- Template End: site/common/WrapOther -->

            </div>
        </section>
    </div>
    <script type="text/javascript" src="<?php echo ROOT_URL; ?>/public/libs/menu/slidebars.js"></script>
    <script>
        (function($) {
            $(document).ready(function() {
                try {
                    $.slidebars();
                } catch (e) {}
            });
        })(jQuery);
    </script>
</body>

</html>