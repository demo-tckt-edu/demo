
                <!-- Template End: site/common/WrapHeader -->

                <div class="block-nopadding">
                    <div class="content noidung menu-desktop">
                        <ul class="table autoLayout absSubMenus" id="ja-cssmenu">
                            <!-- Template Start: site/common/WrapMenu -->
                            <li>
                                <a href="<?php echo ROOT_URL; ?>"><i class="fa fa-home fa-1-5" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a href="#"><span class="menu-title"> GIỚI THIỆU </span></a>
                                <ul>
                                    <li>
                                        <a href="#"><span class="menu-title">Sứ mạng - Tầm nhìn </span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span class="menu-title">Chương trình hành động  </span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span class="menu-title">Cơ cấu tổ chức </span></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo ROOT_URL; ?>/post/"><span class="menu-title">Cơ sở vật chất </span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span class="menu-title">Biểu trưng ý nghĩa </span></a>
                                    </li>                          
                                </ul>
                            </li>
                            <li>
                                <a href="#"><span class="menu-title">ĐẠI HỌC </span></a>
                                <ul>
                                    <li>
                                        <a href="#"><span class="menu-title">Thông Báo </span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span class="menu-title">Ngành đào tạo  </span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span class="menu-title">Chương trình đào tạo </span></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo ROOT_URL; ?>/post/P_list"><span class="menu-title">Quy chế , Quy định</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span class="menu-title">Lịch học vụ </span></a>
                                    </li>
                                     <li>
                                        <a href="#"><span class="menu-title">Tốt nghiệp </span></a>
                                    </li>                             
                                </ul>
                            </li>
                            <li>
                                <a href="#"><span class="menu-title">SAU ĐẠI HỌC </span></a>
                                <ul>
                                    <li>
                                        <a href="#"><span class="menu-title">Thông Báo</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span class="menu-title">Biểu mẫu </span></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo ROOT_URL; ?>/post/P_list"><span class="menu-title">Quy chế,quy định</span></a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><span class="menu-title">NGHIÊN CỨU KHOA HỌC </span></a>
                                <ul>
                                    <li>
                                        <a href="#"><span class="menu-title">Thông báo </span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span class="menu-title">Tạp chí </span></a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><span class="menu-title">TUYỂN SINH </span></a>
                                <ul>
                                    <li>
                                        <a href="<?php echo ROOT_URL; ?>/post/"><span class="menu-title">Thông tin tuyển sinh</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span class="menu-title">Quy chế tuyển sinh </span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span class="menu-title">Kết quả tuyển sinh </span></a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><span class="menu-title">Khảo thí & ĐBCL</span></a>
                                <ul>
                                    <li>
                                        <a href="#"><span class="menu-title">Thông báo</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span class="menu-title">Biểu mẫu - Hướng dẫn</span></a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><span class="menu-title">Văn bản</span></a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><span class="menu-title">GDTX</span></a>
                            </li>
                            <li>
                                <a href="#"><span class="menu-title">Cán bộ - giảng viên</span></a>
                                <ul id="noi-bo">
                                    <li>
                                        <a href="#"><span class="menu-title">Tin tức - thông báo</span></a>
                                    </li>
                                    <li>
                                        <a href="#"><span class="menu-title">Văn bản - Tài liệu</span></a>
                                    </li>
                                     <li>
                                        <a href="#"><span class="menu-title">Lịch coi thi - Lịch giảng dạy</span></a>
                                    </li>
                                     <li>
                                        <a href="#"><span class="menu-title">Công cụ</span></a>
                                        <ul>
                                             <li>
                                                <a href="#"><span class="menu-title">In giấy đề nghị tạm ứng</span></a>
                                            </li>
                                             <li>
                                                <a href="#"><span class="menu-title">Tìm giảng đường rãnh</span></a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li id="topmenu1">
                                <a href="#" target="_blank"><span class="menu-title">Sinh Viên </span></a>
                                 <ul>
                                             <li>
                                                <a href="#"><span class="menu-title">Tin tức - Thông báo</span></a>
                                            </li>
                                             <li>
                                                <a href="#"><span class="menu-title">Học phí</span></a>
                                            </li>
                                            <li>
                                                <a href="#"><span class="menu-title">Học bổng</span></a>
                                            </li>
                                             <li>
                                                <a href="#"><span class="menu-title">Biểu mẫu, hướng dẫn</span></a>
                                            </li>
                                            <li>
                                                <a href="#"><span class="menu-title">Quy chế, quy định</span></a>
                                            </li>
                                             <li>
                                                <a href="#"><span class="menu-title">Kết quả học tập</span></a>
                                            </li>
                                             <li>
                                                <a href="#"><span class="menu-title">Văn hóa - Văn nghệ</span></a>
                                            </li>
                                             <li>
                                                <a href="#"><span class="menu-title">Thông tin tuyển dụng</span></a>
                                            </li>
                                    </ul>
                             </li>
                            <!-- Template End: site/common/WrapMenu -->
                        </ul>
                    </div>
                </div>