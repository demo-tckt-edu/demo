<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi-vn" lang="vi-vn">

<head profile="http://www.w3.org/1999/xhtml/vocab">

    <meta name="google-site-verification" content="" />
    <meta name="robots" content="index, follow" />
    <meta name="revisit-after" content="1 days" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <title>Trường Đại Học Tài Chính Kế Toán </title>
    <meta name="keywords" content="dai hoc, dai hoc cong nghe, đại học, truong dai hoc, trường Đại Học,dai hoc chinh quy, dai hoc lien thong, đại học chính quy, tuyen sinh dai hoc, tuyển sinh đại học, dai hoc,dai hoc cong nghe,dai hoc,truong dai hoc,truong dai hoc,dai hoc chinh quy,dai hoc lien thong,dai hoc chinh quy,tuyen sinh dai hoc,tuyen sinh dai hoc" />
    <meta name="description" content="Trường Đại học uy tín chất lượng hàng đầu Việt Nam, đào tạo đa ngành nghề, đảm bảo thực tập, việc làm cho sinh viên theo học." />
    <meta name="copyright" content="Trường Đại Học Tài Chính Kế Toán" />
    <meta name="author" content="Trường Đại Học Tài Chính Kế Toán" />
    <meta http-equiv="audience" content="General" />
    <meta name="resource-type" content="Document" />
    <meta name="distribution" content="Global" />
    <meta property="og:title" content="Trường Đại Học Tài Chính Kế Toán" />
    <meta property="og:description" content="Trường Đại học uy tín chất lượng hàng đầu Việt Nam, đào tạo đa ngành nghề, đảm bảo thực tập, việc làm cho sinh viên theo học." />
    <meta property="og:site_name" content="#" />
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="vi_VN" />
    
   <link rel="shortcut icon" href="<?php echo ROOT_URL; ?>/public/images/logo.png" type="image/x-icon" />
   
    <script type="text/javascript" src="<?php echo ROOT_URL; ?>/public/js/angular.min.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL; ?>/public/js/ui-mask.js"></script>
    <script src="<?php echo ROOT_URL; ?>/public/js/ui-bootstrap-tpls-1.3.3.js"></script>
   

    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL; ?>/public/css/style.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="<?php echo ROOT_URL; ?>/public/js/0.2.7.js"></script>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">
    <script type="text/javascript" src="<?php echo ROOT_URL; ?>/public/libs/menu/ddaccordion.js"></script>
    <div id="sb-site">
        <div class="wrap-back-to-top">
            <a href="#" class="cd-top"><img src="#" /></a>
        </div>
        <section class="col-sm-12 content nopadding">
            <div class="container nopadding">
                <!-- Template Start: site/common/WrapHeader -->
                <div class="block-nopadding back-blue" id="menumobile">
                    <div class="content noidung">
                        <div class="col-xs-6 nopadding menumobile-left">
                            <span id="openmenu" class="sb-open-left"><i class="fa fa-bars" aria-hidden="true"></i>&nbsp;Danh mục</span>
                            <span id="closemenu" class="sb-open-left2"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Danh mục</span>
                        </div>
                        <div class="col-xs-6 nopadding text-right">
                            <div class="language">

                                <a href="#">
                                    <img class="icon_vn" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif">
                                </a>
                                &nbsp;
                                <a href="#">
                                    <img class="icon_en" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif">
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="block nopadding">
                    <div class="content noidung back-white">
                        <div id="logo" class="headertop">
                            <div class="nopadding header-left">
                                <a href="<?php echo ROOT_URL; ?>">
                                    <img class="img-responsive" src="<?php echo ROOT_URL; ?>/public/images/TaiChinhKeToan.png" />
                                </a>
                            </div>
                            <div class="nopadding header-right">

                                <div class="nopadding header-right-content text-right">
                                    <div class="language">
                                        <p>
                                            <a href="#">
                                                <img class="icon_vn" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif">
                                            </a>
                                            <a href="#">Tiếng Việt</a> |
                                            <a href="#">
                                                <img class="icon_en" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif">
                                            </a>
                                            <a href="#">English</a>
                                        </p>
                                    </div>
                                    <div class="input-group stylish-input-group">
                                        <span id="group" style="display:none;">root</span>
                                        <input type="text" name="searchword" id="qtukhoa1" class="form-control inputbox search-query" placeholder="Tìm kiếm" onblur="if (this.value == '')
                                        this.value = 'Tìm kiếm ';" onfocus="if (this.value == 'Tìm kiếm ')
                                                    this.value = '';" />
                                        <span class="input-group-addon">
                                <button id="btnsearch" type="button" onclick="check_search()">

                                </button>
                            </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>