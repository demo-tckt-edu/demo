 <div class="block-nopadding">
                    <div class="content noidung">
                        <div class="col-sm-9 no-padding-left padding-right-5">
                            <div class="block-content nopadding">
                                <div class="header-block">
                                    <img class="icon-tin-tuyen-sinh" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif" />

                                    <a href="#" class="title-block text-blue">
                            Tin Đại Học Tài Chính - Kế Toán
                        </a>

                                </div>
                                <div class="content-block height-block">
                                    <div class="col-sm-12 nopadding">
                                        <div class="col-sm-9 news-block-left">
                                            <div class="image-news-home">
                                                <a href="#" class="link-img">

                                                    <img class="img-responsive" alt="#" src="<?php echo ROOT_URL; ?>/public/images/KND_1.JPG" />

                                                </a>
                                            </div>
                                            <a href="#" class="news-title text-blue">
                                             Chi bộ Khoa Hệ thống thông tin quản lý tổ chức kết nạp đảng viên mới
                                 </a>
                                            <p class="des_news">
                                               Chiều ngày 26/9/2018 chi bộ Khoa Hệ thống thông tin quản lý tổ chức kết nạp đảng cho quần chúng Nguyễn Thị Thu Hằng - Giảng viên Khoa Hệ thống thông tin quản lý. Đến tham dự buổi lễ có đồng chí Nguyễn Duy Tạo - Ủy viên Ban Thường vụ Đảng ủy Trường, Phó Hiệu trưởng Nhà trường, đại diện các đoàn thể và toàn thể đảng viên của chi bộ.    

                                               </p>
                                        </div>
                                        <div class="col-sm-3 news-block-right">
                                            <a href="#" class="link-img">

                                                <img class="img-responsive" alt="" src="<?php echo ROOT_URL; ?>/public/images/Trungthu_2018.jpg" />
                                            </a>
                                            <a href="#" class="title text-bold wow slideInUp animated">
                                       Câu lạc bộ “We are family” với chương trình “Trung thu nhân ái, vầng trăng yêu thương”
                                    </a>
                                            <a href="#" class="title wow slideInUp animated">
                                      Khoa Quản trị Kinh doanh tổ chức Hội nghị Tổng kết năm học 2017 - 2018
                                    </a>
                                            <a href="#" class="title wow slideInUp animated">
                                       Đoàn viên Thanh niên Trường Đại học Tài chính - Kế toán xung kích tình nguyện tham gia hiến máu nhân đạo
                                    </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 news-block-bottom">
                                        <a href="#" class="col-sm-3 link-img  title ">
                                            <div class="img">
                                                <img class="img-responsive" alt="#" src="<?php echo ROOT_URL; ?>/public/images/kgiang2018.jpg" />
                                            </div>
                                          Rộn ràng các tân sinh viên khóa mới nhập trường
                                        </a>
                                        <a href="#" class="col-sm-3 link-img  title ">
                                            <div class="img">

                                                <img class="img-responsive" alt=#" src="<?php echo ROOT_URL; ?>/public/images/KKT125_2.JPG" />
                                            </div>
                                          Lễ khai giảng khóa bồi dưỡng kế toán trưởng K125/2018
                                        </a>
                                        <a href="#" class="col-sm-3 link-img  title ">
                                            <div class="img">

                                                <img class="img-responsive" alt=" Lễ khai giảng khóa 7 đại học hệ chính quy và khóa 2 đào tạo trình độ thạc sĩ tuyển sinh năm 2018" src="<?php echo ROOT_URL; ?>/public/images/Hình 1.JPG" />
                                            </div>
                                           Lễ khai giảng khóa 7 đại học hệ chính quy và khóa 2 đào tạo trình độ thạc sĩ tuyển sinh năm 2018
                                        </a>
                                        <a href="#" class="col-sm-3 link-img  title end-block">
                                            <div class="img">

                                                <img class="img-responsive" alt="  Đón các lưu sinh viên Lào Khóa 7 đại học" src="<?php echo ROOT_URL; ?>/public/images/LaoK7_1.jpg" />
                                            </div>
                                            Đón các lưu sinh viên Lào Khóa 7 đại học
                                        </a>

                                    </div>
                                    <div class="col-sm-12 text-right padding">

                                        <a href="#" class="text-red text-bold">
                                            <i class="fa fa-plus-square fa-1 text-red" aria-hidden="true"></i> Xem tất cả
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 nopadding">
                            <div class="block-content nopadding">
                                <div class="header-block">
                                    <img class="icon-he-thong-dao-tao" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif" />
                                    <span class="title-block text-blue">Hệ thống đào tạo</span>
                                </div>
                                <div class="content-block height-block he-thong-dao-tao">
                                    <div id="MAUXANHDUONG">
                                        <a href="#">
                                            <img class="iconchild icon-he-thong-dao-tao-tien-sy" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif">
                                            <span>Tuyển sinh Tiến sĩ</span>
                                        </a>
                                    </div>
                                    <div id="MAUXANHTROI">
                                        <a href="#">
                                            <img class="iconchild icon-he-thong-dao-tao-thac-sy" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif">
                                            <span>Tuyển sinh Thạc sĩ</span>
                                        </a>
                                    </div>
                                    <div id="MAUHONG">
                                        <a href="#">
                                            <img class="iconchild icon-he-thong-dao-tao-dai-hoc" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif">
                                            <span>Tuyển sinh Đại học</span>
                                        </a>
                                    </div>
                                    <div id="MAUCAM">
                                        <a href="#">
                                            <img class="iconchild icon-he-thong-dao-tao-cu-nhan-thuc-hanh" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif">
                                            <span>Kỹ sư thực hành</span>
                                        </a>
                                    </div>
                                    <div id="MAUTIM">
                                        <a href="#">
                                            <img class="iconchild icon-he-thong-dao-tao-quoc-te" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif">
                                            <span>Đào tạo Quốc tế</span>
                                        </a>
                                    </div>
                                    <div id="MAUDO">
                                        <a href="#">
                                            <img class="iconchild icon-he-thong-dao-tao-chuan-nhat-ban" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif">
                                            <span>Đại học chuẩn Nhật Bản</span>
                                        </a>
                                    </div>
                                    <div id="MAUXANHDUONGDAM">
                                        <a href="#">
                                            <img class="iconchild icon-he-thong-dao-tao-lien-thong" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif">
                                            <span>Liên thông - Bằng 2</span>
                                        </a>
                                    </div>

                                    <div class="text-center">
                                        <a href="#"><img src="<?php echo ROOT_URL; ?>/public/libs/slider/bannerright.jpg" /></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-nopadding">
                    <div class="content noidung">
                        <div class="col-sm-9 no-padding-left padding-right-5">

                            <div class="block-content nopadding">
                                <div class="header-block">
                                    <img class="icon-tin-hutech" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif" />

                                    <a href="#" class="title-block text-blue">
                            Hoạt động sinh viên
                        </a>

                                </div>
                                <div class="content-block height-block">
                                    <div class="col-sm-12 nopadding height-block-2">
                                        <div class="col-sm-9 news-block-left">
                                            <div class="image-news-home">
                                                <a href="#" class="link-img">
                                                    <img class="img-responsive" alt="  Kế hoạch tổ chức hội thi Tài năng kế toán năm 2018" src="https://www.hutech.edu.vn/img/news/coverkientrucxanh-1538034554.jpg" />

                                                </a>
                                            </div>
                                            <a href="#" class="news-title text-blue">
                               Kế hoạch tổ chức hội thi "Tài năng kế toán" năm 2018
                            </a>
                                            <p class="des_news">
                                                Nối dài loạt thành tích ấn tượng của sinh viên ngành Kiến trúc trường Đại Học Tài Chính-Kế Toán , thêm hai “chú kiến”  vừa xuất sắc giành giải tại sân chơi Kiến trúc Xanh sinh viên - cuộc thi nằm trong khuôn khổ Tuần lễ Kiến trúc Xanh Việt Nam 2018. </p>
                                        </div>
                                        <div class="col-sm-3 news-block-right">
                                            <a href="#" class="link-img">

                                                <img class="img-responsive" alt="Bạn đã “Book vé” gặp gỡ CEO Tập đoàn Asanzo và Grab Việt Nam vào ngày 03/10 chưa?" src="<?php echo ROOT_URL; ?>/public/images/airplane-5-icon.png"style="
                                                                                                                                width: 153px;
                                                                                                                                height: 94px;
                                                                                                                            "  />
                                            </a>
                                            <a href="#" class="title text-bold wow slideInUp animated">
                                        Bạn đã “Book vé” gặp gỡ CEO Tập đoàn Asanzo và Grab Việt Nam vào ngày 03/10 chưa?
                                    </a>
                                            <a href="#" class="title wow slideInUp animated">
                                        Đăng ký tham gia chương trình “Thực tập sinh tiềm năng Sacombank 2019”
                                    </a>
                                            <a href="#" class="title wow slideInUp animated">
                                        Đại Học Tài Chính-Kế Toán  khai mạc hội thao chào đón Tân sinh viên 2018 -2019
                                    </a>
                                            <a href="#" class="title wow slideInUp animated">
                                        Đặc sắc chương trình giao lưu cùng 02 đoàn nghệ thuật đến từ đất nước Nhật Bản
                                    </a>

                                        </div>
                                    </div>
                                    <div class="col-sm-12 news-block-bottom">
                                        <a href="#" class="col-sm-3 link-img  title ">
                                            <div class="img">

                                                <img class="img-responsive" alt="Tân sinh viên Kế toán - Tài chính - Ngân hàng định hướng tương lai cùng các chuyên gia" src="<?php echo ROOT_URL; ?>/public/images/KNDSV_1.JPG" />
                                            </div>
                                          Chi bộ Công tác sinh viên tổ chức kết nạp đảng viên mới
                                        </a>
                                        <a href="#" class="col-sm-3 link-img  title ">
                                            <div class="img">

                                                <img class="img-responsive" alt="Đoàn viên Thanh niên Trường Đại học Tài chính - Kế toán xung kích tình nguyện tham gia hiến máu nhân đạo" src="<?php echo ROOT_URL; ?>/public/images/hienmau_3.jpg" />
                                            </div>
                                          Đoàn viên Thanh niên xung kích tình nguyện tham gia hiến máu nhân đạo
                                        </a>
                                        <a href="#" class="col-sm-3 link-img  title ">
                                            <div class="img">

                                                <img class="img-responsive" alt="Tân sinh viên khoa Xây Dựng sôi nổi hòa nhịp với “Hành trình người kỹ sư Xây Dựng”" src="<?php echo ROOT_URL; ?>/public/images/thitieng-anh.jpg" />
                                            </div>
                                          tổ chức học bổ sung kiến thức Tiếng Anh cho sinh viên khóa 7 đại học hệ chính quy
                                        </a>
                                        <a href="#" class="col-sm-3 link-img  title end-block">
                                            <div class="img">

                                                <img class="img-responsive" alt="  Đại Học Tài Chính -Kế Toán  ơi, Trung thu đã đi đâu thế?" src="<?php echo ROOT_URL; ?>/public/images/maxresdefault.png" />
                                            </div>
                                            Đại Học Tài Chính -Kế Toán  ơi, Trung thu đã đi đâu thế?
                                        </a>

                                    </div>
                                    <div class="col-sm-12 text-right padding">

                                        <a href="#" class="text-red text-bold">
                                            <i class="fa fa-plus-square fa-1 text-red" aria-hidden="true"></i> Xem tất cả
                                        </a>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-3 nopadding">
                            <div class="block-content nopadding">
                                <div class="header-block">
                                    <img class="icon-tin-noi-bat" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif" />
                                    <span class="title-block text-red">Tin nổi bật</span>
                                </div>
                                <div class="content-block height-block">
                                    <div class="height-block-2">
                                        <ul class="latestnews">

                                            <li class="wow slideInUp animated">
                                                <a class="latestnews" href="#">
                                Hướng dẫn sơ đồ giữ xe dành cho sinh viên Đại Học Tài Chính -Kế Toán
                                <img src="https://www.hutech.edu.vn/s-img/newicon_vi.gif">
                            </a>
                                            </li>
                                            <li class="wow slideInUp animated">
                                                <a class="latestnews" href="#">
                                Hướng dẫn cách xem thời khoá biểu cho Tân sinh viên
                                <img src="https://www.hutech.edu.vn/s-img/newicon_vi.gif">
                            </a>
                                            </li>
                                            <li class="wow slideInUp animated">
                                                <a class="latestnews" href="#">
                                Lộ trình Đại Học Tài Chính -Kế Toán bus dành cho Sinh viên tại TT Đào tạo nhân lực Chất lượng cao
                                <img src="https://www.hutech.edu.vn/s-img/newicon_vi.gif">
                            </a>
                                            </li>
                                            <li class="wow slideInUp animated">
                                                <a class="latestnews" href="#">
                                 Chức năng của các phòng/ Ban hỗ trợ sinh viên
                                <img src="https://www.hutech.edu.vn/s-img/newicon_vi.gif">
                            </a>
                                            </li>
                                            <li class="wow slideInUp animated">
                                                <a class="latestnews" href="#">
                               Tin Tức Thông Báo
                                <img src="https://www.hutech.edu.vn/s-img/newicon_vi.gif">
                            </a>
                                            </li>
                                            <li class="wow slideInUp animated">
                                                <a class="latestnews" href="#">
                                Tư vấn tuyển sinh trực tuyến
                                <img src="https://www.hutech.edu.vn/s-img/newicon_vi.gif">
                            </a>
                                            </li>
                                            <li class="wow slideInUp animated">
                                                <a class="latestnews" href="#">
                                Thông tin tuyển dụng
                                <img src="https://www.hutech.edu.vn/s-img/newicon_vi.gif">
                            </a>
                                            </li>
                                        </ul>
                                        <div class="header-block sinh-vien-khoi-nghiep">
                                            <img class="icon-sinh-vien-khoi-nghiep" src="https://www.hutech.edu.vn/images/img_blank.gif" />
                                            <span class="title-block text-blue">Sinh viên khởi nghiệp</span>
                                        </div>
                                    </div>  

                                    <div class="col-sm-12 news-block-bottom">
                                        <a href="#" class="col-sm-12 link-img nopadding  title title2 " style="
                                                                                                        margin-bottom: 20px;
                                                                                                    ">
                                            <div class="img2">

                                                <img class="img-responsive" alt="Thú vị câu chuyện khởi nghiệp bằng “Tranh vảy cá” của sinh viên Đại học HUTECH" src="<?php echo ROOT_URL; ?>/public/images/a1_ostr.jpg" />
                                            </div>
                                            Thú vị câu chuyện khởi nghiệp bằng “Tranh vảy cá” của sinh viên 
                                            Đại Học Tài Chính -Kế Toán
                                        </a>

                                    </div>
                                    <div class="col-sm-12 text-right padding">

                                        <a href="#" class="text-red text-bold">
                                            <i class="fa fa-plus-square fa-1 text-red" aria-hidden="true"></i> Xem tất cả
                                        </a>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-nopadding wow fadeIn">
                    <div class="content noidung back-white border-radius">
                        <div class="header-block">
                            <div class="title-line-right title-line-orange">
                                <img class="icon-media" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif" />
                                <a href=#" class="title-block text-orange">MEDIA</a>
                            </div>
                        </div>
                        <div class="content-block">
                            <div class="col-sm-12 nopadding wow slideInRight animated">
                                <div class="col-xs-6 col-md-3 col-sm-3 thumb-video">
                                    <a href="#" class="text-bold">
                                        <div class="img">
                                            <img class="img-responsive thumb" src="<?php echo ROOT_URL; ?>/public/images/KKT125_2.JPG" />
                                            <img data-brackets-id="18211" class="icon-media-play" src="https://www.hutech.edu.vn/images/img_blank.gif" />
                                        </div>
                                       Lễ khai giảng khóa bồi dưỡng kế toán trưởng K125/2018
                                    </a>
                                </div>
                                <div class="col-xs-6 col-md-3 col-sm-3 thumb-video">
                                    <a href="#" class="text-bold">
                                        <div class="img">
                                            <img class="img-responsive thumb" src="<?php echo ROOT_URL; ?>/public/images/KND_3.JPG" />
                                            <img data-brackets-id="18211" class="icon-media-play" src="https://www.hutech.edu.vn/images/img_blank.gif" />
                                        </div>
                                        Chi bộ Khoa Hệ thống thông tin quản lý tổ chức kết nạp đảng viên mới
                                    </a>
                                </div>
                                <div class="col-xs-6 col-md-3 col-sm-3 thumb-video">
                                    <a href="#">
                                        <div class="img">
                                            <img class="img-responsive thumb" src="<?php echo ROOT_URL; ?>/public/images/hienmau_5.jpg" />
                                            <img data-brackets-id="18211" class="icon-media-play" src="https://www.hutech.edu.vn/images/img_blank.gif" />
                                        </div>
                                      Đoàn viên Thanh niên Trường Đại học Tài chính - Kế toán xung kích tình nguyện tham gia hiến máu nhân đạo
                                    </a>
                                </div>
                                <div class="col-xs-6 col-md-3 col-sm-3 thumb-video">
                                    <a href="#">
                                        <div class="img">
                                            <img class="img-responsive thumb" src="<?php echo ROOT_URL; ?>/public/images/LaoK7_1 (1).jpg" />
                                            <img data-brackets-id="18211" class="icon-media-play" src="https://www.hutech.edu.vn/images/img_blank.gif" />
                                        </div>
                                      Đón các lưu sinh viên Lào Khóa 7 đại học
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block">
                    <div class="content noidung">
                        <div class="col-sm-9 no-padding-left padding-right-5">
                            <div class="block-content nopadding">
                                <div class="header-block">
                                    <img class="icon-hoat-dong-sinh-vien" src="<?php echo ROOT_URL; ?>/public/images/img_blank.gif" />
                                    <a href="#" class="news-title text-blue">
                                  Tin tuyển sinh
                        </a>

                                </div>
                                <div class="content-block height-block">
                                    <div class="col-sm-12 nopadding">
                                        <div class="col-sm-9 news-block-left">
                                            <div class="image-news-home">
                                                <a href="#" class="link-img">

                                                    <img class="img-responsive" alt="     Đại học Tài chính - Kế toán thông báo lịch ôn thi tuyển sinh cao học năm 2018 - đợt 2" src="https://www.hutech.edu.vn/img/news/coversaudaihoc-1536810363.jpg" />
                                                </a>
                                            </div>
                                            <a href="#" class="news-title text-blue">
                               Đại học Tài chính - Kế toán thông báo lịch ôn thi tuyển sinh cao học năm 2018 - đợt 2
                            </a>
                                            <p class="des_news">
                                                Theo thông báo từ Hội đồng tuyển sinh cao học  Đại học Tài chính - Kế toán, các thí sinh tham dự Kỳ thi tuyển sinh trình độ Thạc sĩ năm 2018 - đợt 2 sẽ tham gia thi tuyển vào các ngày 27 & 28/10/2018. </p>
                                        </div>
                                        <div class="col-sm-3 news-block-right">
                                            <a href="#" class="link-img">

                                                <img class="img-responsive" alt="  Đại học Tài chính - Kế toán tuyển sinh Liên thông và Đại học chính quy VB2 năm 2018 - đợt 2" src="https://www.hutech.edu.vn/img/news/cover1-1533634288.jpg" />
                                            </a>
                                            <a href="#" class="title text-bold wow slideInUp animated">
                                         Đại học Tài chính - Kế toán tuyển sinh Liên thông và Đại học chính quy VB2 năm 2018 - đợt 2
                                    </a>
                                            <a href="#" class="title wow slideInUp animated">
                                       Đại học Tài chính - Kế toán tuyển sinh đào tạo trình độ Thạc sĩ năm 2018 - Đợt 2
                                    </a>
                                            <a href="#" class="title wow slideInUp animated">
                                        Thấy gì từ điểm chuẩn các trường đại học năm 2018?
                                    </a>
                                            <a href="#" class="title wow slideInUp animated">
                                        Điểm chuẩn trường đại học công - tư gần nhau
                                    </a>

                                        </div>
                                    </div>
                                    <div class="col-sm-12 news-block-bottom">
                                        <a href="#" class="col-sm-3 link-img  title ">
                                            <div class="img">
                                                <img class="img-responsive" alt="Học 1 năm lấy bằng Cử nhân Quản trị nhà hàng của ĐH Pháp ngay tại Việt Nam" src="https://www.hutech.edu.vn/img/news/avatar_cergy_pontoise-1536552160.jpg" />
                                            </div>
                                            Học 1 năm lấy bằng Cử nhân Quản trị nhà hàng của ĐH Pháp ngay tại Việt Nam
                                        </a>
                                        <a href="#" class="col-sm-3 link-img  title ">
                                            <div class="img">

                                                <img class="img-responsive" alt=" công bố điểm trúng tuyển học bạ đợt 31/8" src="<?php echo ROOT_URL; ?>/public/images/diem-chuan-dai-hoc-2018-cua-truong-dai-hoc-tai-chinh-ke-toan-0.jpg" />
                                            </div>
                                             Đại học Tài chính - Kế toán công bố điểm trúng tuyển học bạ đợt 31/8
                                        </a>
                                        <a href="#" class="col-sm-3 link-img  title ">
                                            <div class="img">

                                                <img class="img-responsive" alt="Hướng dẫn cách xem Thời khóa biểu cho Tân sinh viên khóa 2018" src="https://www.hutech.edu.vn/img/news/coverthoikhoabieu-1535174704.jpg" />
                                            </div>
                                            Hướng dẫn cách xem Thời khóa biểu cho Tân sinh viên khóa 2018
                                        </a>
                                        <a href="#" class="col-sm-3 link-img  title end-block">
                                            <div class="img">

                                                <img class="img-responsive" alt=" Đại học Tài chính - Kế toán công bố điểm nhận hồ sơ xét tuyển học bạ đợt 31/8" src="<?php echo ROOT_URL; ?>/public/images/201701_TVTS-H2.jpg" />
                                            </div>
                                             Đại học Tài chính - Kế toán công bố điểm nhận hồ sơ xét tuyển học bạ đợt 31/8
                                        </a>

                                    </div>
                                    <div class="col-sm-12 text-right padding">
                                        <a href="#" class="text-red text-bold">
                                            <i class="fa fa-plus-square fa-1 text-red" aria-hidden="true"></i> Xem tất cả
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 nopadding">
                            <div class="block-content nopadding">
                                <div class="header-block">
                                    <img class="icon-lich-su-kien" src="https://www.hutech.edu.vn/images/img_blank.gif" />
                                    <span class="title-block text-blue">Lịch sự kiện</span>
                                </div>
                                <div class="content-block height-block">
                                    <div class="col-xs-12 nopadding event">
                                        <div class="col-xs-4 nopadding event-left">
                                            <div class="w-full back-orange text-white text-bold">Thứ 7</div>
                                            <div class="w-full">06/10/2018</div>
                                        </div>
                                        <div class="col-xs-8 no-padding-right-2 event-right">
                                            <div class="text-justify">
                                                ĐÊM HỘI VĂN HÓA CHÀO ĐÓN TÂN SINH VIÊN ĐẠI HỌC TÀI CHÍNH - KẾ TOÁN 2019
                                            </div>

                                            <div class="text-orange">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i> La Hà, Tư Nghĩa, Quảng Ngãi
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 nopadding event">
                                        <div class="col-xs-4 nopadding event-left">
                                            <div class="w-full back-orange text-white text-bold">Thứ 7</div>
                                            <div class="w-full">22/09/2018</div>
                                        </div>
                                        <div class="col-xs-8 no-padding-right-2 event-right">
                                            <div class="text-justify">
                                                LỄ KHAI GIẢNG CHƯƠNG TRÌNH ĐÀO TẠO K2018
                                            </div>

                                            <div class="text-orange">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i> La Hà, Tư Nghĩa, Quảng Ngãi
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 nopadding event">
                                        <div class="col-xs-4 nopadding event-left">
                                            <div class="w-full back-orange text-white text-bold">Thứ 7</div>
                                            <div class="w-full">15/09/2018</div>
                                        </div>
                                        <div class="col-xs-8 no-padding-right-2 event-right">
                                            <div class="text-justify">
                                                LỄ BẾ GIẢNG VÀ TRAO BẰNG TỐT NGHIỆP ĐH - CĐ CHÍNH QUY
                                            </div>

                                            <div class="text-orange">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i> La Hà, Tư Nghĩa, Quảng Ngãi
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 nopadding event">
                                        <div class="col-xs-4 nopadding event-left">
                                            <div class="w-full back-orange text-white text-bold">Thứ 6</div>
                                            <div class="w-full">14/09/2018</div>
                                        </div>
                                        <div class="col-xs-8 no-padding-right-2 event-right">
                                            <div class="text-justify">
                                                LỄ KHAI GIẢNG NĂM HỌC 2018 - 2019 VÀ PHÁT ĐỘNG PHONG TRÀO NCKH, SV 5 TỐT, LỚP HỌC TIÊN TIẾN
                                            </div>

                                            <div class="text-orange">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i> La Hà, Tư Nghĩa, Quảng Ngãi
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 nopadding event">
                                        <div class="col-xs-4 nopadding event-left">
                                            <div class="w-full back-orange text-white text-bold">Thứ 6</div>
                                            <div class="w-full">07/09/2018</div>
                                        </div>
                                        <div class="col-xs-8 no-padding-right-2 event-right">
                                            <div class="text-justify">
                                                LỄ KHAI GIẢNG CHƯƠNG TRÌNH ĐÀO TẠO CHUẨN QUỐC TẾ
                                            </div>

                                            <div class="text-orange">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i> La Hà, Tư Nghĩa, Quảng Ngãi
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <style>
                    .no-padding-home {
                        padding: 0 !important;
                    }
                </style>