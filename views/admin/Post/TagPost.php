<?php
include 'views/admin/layoutAdmin/head.php';
include 'views/admin/layoutAdmin/header.php';
?>
<!-- slidebar #end header -->

<!-- main-container -->
<div class="main-container clearfix">
    <!-- main-navigation -->
    <?php include 'views/admin/layoutAdmin/sidebar.php';  ?>
    <!-- #end main-navigation -->

    <!-- content-here -->
    <div class="content-container nav-expand fixedHeader" id="content">
        <div class="page page-forms-elements">

            <ol class="breadcrumb breadcrumb-small">
                <li>Post</li>
                <li class="active"><a href="forms.elements.html">Add New</a></li>
            </ol>

            <div class="page-wrap">
                <!-- row -->
                <div class="row">

                    <!-- col-left -->
                    <div class="col-md-5">
                        <div class="panel panel-default panel-hovered panel-stacked mb30">
                            <div class="panel-heading">Add New Category</div>
                            <div class="panel-body">
                                <form role="form" class="form-horizontal" action="javascript:;"> <!-- form horizontal acts as a row -->
                                    <!-- normal control -->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Name</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control">
                                            <h6 class="control-label">The name is how it appears on your site.</h6>
                                        </div>

                                    </div>

                                    <!-- with hint -->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Slug</label>
                                        <div class="col-md-9">
                                            <input type="text" class="form-control">
                                            <h6 class="control-label">The “slug” is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</h6>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">None</label>
                                        <div class="col-md-9">
                                            <select class="form-control">
                                                <option value="0">None</option>
                                                <option value="1">Dịch vụ</option>
                                                <option value="2">Tin tức</option>
                                                <option value="3">Shop Manager</option>

                                            </select>
                                            <p class="control-label">Categories, unlike tags, can have a hierarchy. You might have a Jazz category, and under that have children categories for Bebop and Big Band. Totally optional.</p>
                                        </div>
                                    </div>

                                    <!-- passowrd control -->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Decscription</label>
                                        <div class="col-md-9">
                                            <textarea name="description" id="tag-description" rows="5" cols="40"></textarea>
                                            <h6  class="control-label">The description is not prominent by default; however, some themes may show it.</h6>
                                        </div>
                                    </div>


                                    <div class="clearfix right">
                                        <button class="btn btn-primary mr5 waves-effect" type="submit">Save</button>
                                        <button class="btn btn-default waves-effect">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> <!-- #end col-left -->

                    <div class="col-md-7">
                        <div class="panel panel-lined panel-hovered mb20 table-responsive">
                            <div class="panel-heading">
                                List Category
                            </div>
                            <div class="panel-body">
                                <table class="table ng-scope" ng-controller="ResponsiveTableDemoCtrl" ng-class="{
              'table-bordered': tableModel.bordered,
              'table-striped': tableModel.striped,
              'table-condensed': tableModel.condensed,
              'table-hover': tableModel.hover }">
                                    <thead>
                                    <tr>
                                        <th>
                                            <input  type="checkbox">
                                        </th>
                                        <th>Name</th>
                                        <th>Decsription</th>
                                        <th>Slug</th>
                                        <th>Count</th>

                                    </tr>

                                </table>
                                <div class="panel-footer clearfix">
                                    <p class="left mt15 small ng-binding">
                                        Showing 1 to 7 of 100 entries
                                    </p>
                                    <ul class="pagination-sm right pagination ng-isolate-scope ng-valid" boundary-links="true" total-items="filteredData.length" ng-model="currentPage" max-size="5" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" previous-text="‹" next-text="›" first-text="«" last-text="»" tabindex="0" aria-invalid="false">
                                        <!-- ngIf: boundaryLinks --><li ng-if="boundaryLinks" ng-class="{disabled: noPrevious()}" class="ng-scope disabled"><a href="" ng-click="selectPage(1)" class="ng-binding" tabindex="0">«</a></li><!-- end ngIf: boundaryLinks -->
                                        <!-- ngIf: directionLinks --><li ng-if="directionLinks" ng-class="{disabled: noPrevious()}" class="ng-scope disabled"><a href="" ng-click="selectPage(page - 1)" class="ng-binding" tabindex="0">‹</a></li><!-- end ngIf: directionLinks -->
                                        <!-- ngRepeat: page in pages track by $index --><li ng-repeat="page in pages track by $index" ng-class="{active: page.active}" class="ng-scope active"><a href="" ng-click="selectPage(page.number)" class="ng-binding" tabindex="0">1</a></li><!-- end ngRepeat: page in pages track by $index --><li ng-repeat="page in pages track by $index" ng-class="{active: page.active}" class="ng-scope"><a href="" ng-click="selectPage(page.number)" class="ng-binding" tabindex="0">2</a></li><!-- end ngRepeat: page in pages track by $index --><li ng-repeat="page in pages track by $index" ng-class="{active: page.active}" class="ng-scope"><a href="" ng-click="selectPage(page.number)" class="ng-binding" tabindex="0">3</a></li><!-- end ngRepeat: page in pages track by $index --><li ng-repeat="page in pages track by $index" ng-class="{active: page.active}" class="ng-scope"><a href="" ng-click="selectPage(page.number)" class="ng-binding" tabindex="0">4</a></li><!-- end ngRepeat: page in pages track by $index --><li ng-repeat="page in pages track by $index" ng-class="{active: page.active}" class="ng-scope"><a href="" ng-click="selectPage(page.number)" class="ng-binding" tabindex="0">5</a></li><!-- end ngRepeat: page in pages track by $index --><li ng-repeat="page in pages track by $index" ng-class="{active: page.active}" class="ng-scope"><a href="" ng-click="selectPage(page.number)" class="ng-binding" tabindex="0">...</a></li><!-- end ngRepeat: page in pages track by $index -->
                                        <!-- ngIf: directionLinks --><li ng-if="directionLinks" ng-class="{disabled: noNext()}" class="ng-scope"><a href="" ng-click="selectPage(page + 1)" class="ng-binding" tabindex="0">›</a></li><!-- end ngIf: directionLinks -->
                                        <!-- ngIf: boundaryLinks --><li ng-if="boundaryLinks" ng-class="{disabled: noNext()}" class="ng-scope"><a href="" ng-click="selectPage(totalPages)" class="ng-binding" tabindex="0">»</a></li><!-- end ngIf: boundaryLinks -->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>









                </div> <!-- #end page-wrap -->
            </div> <!-- #end page -->
        </div>

    </div> <!-- #end main-container -->
    <?php include 'views/admin/layoutAdmin/footer.php';  ?>
