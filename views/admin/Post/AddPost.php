<?php
include 'views/admin/layoutAdmin/head.php';
include 'views/admin/layoutAdmin/header.php';

?>

<!-- slidebar #end header -->

<!-- main-container -->
<div class="main-container clearfix">
    <!-- main-navigation -->
    <?php include 'views/admin/layoutAdmin/sidebar.php';  ?>
    <!-- #end main-navigation -->

    <!-- content-here -->
    <div class="content-container nav-expand fixedHeader" id="content">
        <div class="page page-forms-elements">

            <ol class="breadcrumb breadcrumb-small">
                <li>Post</li>
                <li class="active"><a href="forms.elements.html">Add New</a></li>
            </ol>

            <div class="page-wrap">
                <!-- row -->
                <div class="row">

                    <!-- col-left -->
                    <div class="col-md-9">
                        <div class="panel panel-default panel-hovered panel-stacked mb30">
                            <div class="panel-heading">Add New Post</div>
                            <div class="panel-body">
                                <form role="form" class="form-horizontal" action="javascript:;"> <!-- form horizontal acts as a row -->
                                    <!-- normal control -->
                                    <div class="form-group">
                                        <label class="control-label">Title</label>
                                        <div class="">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Content</label>
                                        <div class="">
                                            <textarea class="col-md-9" name="post_content" id="post_content" rows="10" cols="150"></textarea>
                                        </div>
                                    </div>



                                    <!-- with hint -->
                                    <div class="form-group">
                                        <label class="control-label">Short Description</label>
                                        <div class="">
                                            <textarea name="description" id="tag-description" rows="5" cols="107"></textarea>

                                        </div>
                                    </div>

                                    <!-- passowrd control -->
                                    <div class="form-group">
                                        <label class="control-label">Tag</label>
                                        <div class="">
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>


                                    <div class="clearfix right">
                                        <button class="btn btn-primary mr5 waves-effect" type="submit">Save</button>
                                        <button class="btn btn-default waves-effect">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> <!-- #end col-left -->
                    <div class="col-md-3">
                        <div class="panel panel-default panel-hovered panel-stacked mb30">
                            <div class="panel-heading">Category</div>
                            <div class="panel-body">
                                <form role="form" class="form-horizontal" action="javascript:;">
                        <div class="form-group">

                            <div class="">
                                <ul>
                                    <li>
                                        <label class="control-label">Dịch vụ</label>
                                        <input type="checkbox"  value="">
                                    </li>
                                    <li>
                                        <label class="control-label">Tin tức</label>
                                        <input type="checkbox"  value="">
                                    </li>
                                    <li>
                                        <label class="control-label">Dịch vụ</label>
                                        <input type="checkbox">
                                    </li>
                                </ul>
                            </div>

                        <!-- Upload Image-->
                        <div class="col-md-3">
                            <label class="control-label">Image</label>

                                <input type="file">

                        </div>
                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                </div>











            </div> <!-- #end page-wrap -->
            </div> <!-- #end page -->
        </div>

    </div> <!-- #end main-container -->
    <?php include 'views/admin/layoutAdmin/footer.php';  ?>
