
	<!-- theme settings -->
	<div class="site-settings clearfix hidden-xs">
		<div class="settings clearfix">
			<div class="trigger ion ion-settings left"></div>
			<div class="wrapper left">
				<ul class="list-unstyled other-settings">
					<li class="clearfix mb10">
						<div class="left small">Nav Horizontal</div>
						<div class="md-switch right">
							<label>
								<input type="checkbox" id="navHorizontal">
								<span>&nbsp;</span>
							</label>
						</div>


					</li>
					<li class="clearfix mb10">
						<div class="left small">Fixed Header</div>
						<div class="md-switch right">
							<label>
								<input type="checkbox"  id="fixedHeader">
								<span>&nbsp;</span>
							</label>
						</div>
					</li>
					<li class="clearfix mb10">
						<div class="left small">Nav Full</div>
						<div class="md-switch right">
							<label>
								<input type="checkbox"  id="navFull">
								<span>&nbsp;</span>
							</label>
						</div>
					</li>
				</ul>
				<hr/>
				<ul class="themes list-unstyled" id="themeColor">
					<li data-theme="theme-zero" class="active"></li>
					<li data-theme="theme-one"></li>
					<li data-theme="theme-two"></li>
					<li data-theme="theme-three"></li>
					<li data-theme="theme-four"></li>
					<li data-theme="theme-five"></li>
					<li data-theme="theme-six"></li>
					<li data-theme="theme-seven"></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- #end theme settings -->


	

	<!-- Dev only -->
	<!-- Vendors -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.js"></script>
	<script src="<?php echo ROOT_URL; ?>/public/admin/scripts/plugins/d3.min.js"></script>
	<script src="<?php echo ROOT_URL; ?>/public/admin/scripts/plugins/c3.min.js"></script>
	<script src="<?php echo ROOT_URL; ?>/public/admin/scripts/plugins/screenfull.js"></script>
	<script src="<?php echo ROOT_URL; ?>/public/admin/scripts/plugins/perfect-scrollbar.min.js"></script>
	<script src="<?php echo ROOT_URL; ?>/public/admin/scripts/plugins/waves.min.js"></script>
	<script src="<?php echo ROOT_URL; ?>/public/admin/scripts/plugins/jquery.sparkline.min.js"></script>
	<script src="<?php echo ROOT_URL; ?>/public/admin/scripts/plugins/jquery.easypiechart.min.js"></script>
	<script src="<?php echo ROOT_URL; ?>/public/admin/scripts/plugins/bootstrap-rating.min.js"></script>
	<script src="<?php echo ROOT_URL; ?>/public/admin/scripts/app.js"></script>
	<script src="<?php echo ROOT_URL; ?>/public/admin/scripts/index.init.js"></script>
    <script>
        // Thay thế <textarea id="post_content"> với CKEditor
        CKEDITOR.replace( 'post_content' );// tham số là biến name của textarea
    </script>