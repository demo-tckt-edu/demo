
<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="Materia - Admin Template">
	<meta name="keywords" content="materia, webapp, admin, dashboard, template, ui">
	<meta name="author" content="solutionportal">
	<!-- <base href="/"> -->

	<title>Materia - Admin Template</title>
	
	<!-- Icons -->
	<link rel="stylesheet" href="<?php echo ROOT_URL; ?>/public/admin/fonts/ionicons/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo ROOT_URL; ?>/public/admin/fonts/font-awesome/css/font-awesome.min.css">

	<!-- Plugins -->
	<link rel="stylesheet" href="<?php echo ROOT_URL; ?>/public/admin/styles/plugins/c3.css">
	<link rel="stylesheet" href="<?php echo ROOT_URL; ?>/public/admin/styles/plugins/waves.css">
	<link rel="stylesheet" href="<?php echo ROOT_URL; ?>/public/admin/styles/plugins/perfect-scrollbar.css">

	
	<!-- Css/Less Stylesheets -->
	<link rel="stylesheet" href="<?php echo ROOT_URL; ?>/public/admin/styles/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo ROOT_URL; ?>/public/admin/styles/main.min.css">


	 
 	<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300' rel='stylesheet' type='text/css'>

	<!-- Match Media polyfill for IE9 -->
	<!--[if IE 9]> <script src="scripts/ie/matchMedia.js"></script>  <![endif]-->

    <script src="<?php echo ROOT_URL; ?>/public/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-50750935-1']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

/* ]]> */
</script>
</head>