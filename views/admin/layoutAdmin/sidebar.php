	<aside class="nav-wrap" id="site-nav" data-perfect-scrollbar>
			<div class="nav-head">
				<!-- site logo -->
				<a href="index.html" class="site-logo text-uppercase">
					<i class="ion ion-disc"></i>
					<span class="text">Materia</span>
				</a>
			</div>

			<!-- Site nav (vertical) -->

			<nav class="site-nav clearfix" role="navigation">
				<div class="profile clearfix mb15">
					<img src="<?php echo ROOT_URL; ?>/public/admin/images/admin.jpg" alt="admin">
					<div class="group">
						<h5 class="name">Robert Smith</h5>
						<small class="desig text-uppercase">UX Designer</small>
					</div>
				</div>

				<!-- navigation -->
				<ul class="list-unstyled clearfix nav-list mb15">
					<li class="active">
						<a href="index.html">
							<i class="ion ion-monitor"></i>
							<span class="text">Dashboard</span>
						</a>
					</li>
					<li class="waves-effect open">
						<a href="javascript:;">
							<i class="ion ion-document-text"></i>
							<span class="text">Users</span>
							<i class="arrow ion-chevron-left"></i>
						</a>
						<ul class="inner-drop list-unstyled">
							<li class=" waves-effect"><a href="<?php echo ROOT_URL; ?>/admin/User/UserList"">All Users</a></li>
						</ul>
						<ul class="inner-drop list-unstyled">
							<li class=" waves-effect"><a href="<?php echo ROOT_URL; ?>/admin/User/AddUser">Add New</a></li>
						</ul>
						<ul class="inner-drop list-unstyled">
							<li class=" waves-effect"><a href="<?php echo ROOT_URL; ?>/admin/User/DetailUser">Your Profile</a></li>
						</ul>
					</li>

                    <li class="waves-effect open">
                        <a href="javascript:;">
                            <i class="ion ion-clipboard"></i>
                            <span class="text">Post</span>
                            <i class="arrow ion-chevron-left"></i>
                        </a>
                        <ul class="inner-drop list-unstyled">
                            <li class=" waves-effect"><a href="<?php echo ROOT_URL; ?>/admin/Post/PostList"">All Posts</a></li>
                        </ul>
                        <ul class="inner-drop list-unstyled">
                            <li class=" waves-effect"><a href="<?php echo ROOT_URL; ?>/admin/Post/AddPost">Add New</a></li>
                        </ul>
                        <ul class="inner-drop list-unstyled">
                            <li class=" waves-effect"><a href="<?php echo ROOT_URL; ?>/admin/Post/CategoryPost">Categories</a></li>
                        </ul>
                        <ul class="inner-drop list-unstyled">
                            <li class=" waves-effect"><a href="<?php echo ROOT_URL; ?>/admin/Post/TagPost">Tag</a></li>
                        </ul>
                    </li>




					<li>
						<a href="javascript:;">
							<i class="ion ion-printer"></i>
							<span class="text">Pages</span>
							<i class="arrow ion-chevron-left"></i>
						</a>
						<ul class="inner-drop list-unstyled">
							<li><a href="pages.signin.html">Sign In</a></li>
							<li><a href="pages.signup.html">Sign Up</a></li>
							<li><a href="pages.forget-pass.html">Forget Pass</a></li>
							<li><a href="pages.404.html">404</a></li>
							<li><a href="pages.lockscreen.html">Lock Screen</a></li>
							<li><a href="pages.timeline.html">Timeline</a></li>
							<li><a href="pages.search.html">Search</a></li>
							<li><a href="pages.invoice.html">Invoice</a></li>
						</ul>
					</li>
				</ul> <!-- #end navigation -->
			</nav>

			<!-- nav-foot -->
			<footer class="nav-foot">
				<p>2015 &copy; <span>MATERIA</span></p>
			</footer>

		</aside>