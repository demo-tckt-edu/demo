<?php
include 'views/admin/layoutAdmin/head.php';
include 'views/admin/layoutAdmin/header.php';
?>
<!-- slidebar #end header -->

<!-- main-container -->
<div class="main-container clearfix">
    <!-- main-navigation -->
    <?php include 'views/admin/layoutAdmin/sidebar.php';  ?>
    <!-- #end main-navigation -->
    <div ng-class="{'nav-expand': navFull, 'fixedHeader': fixedHeader}" class="content-container ng-scope nav-expand fixedHeader" id="content" ng-view=""><div class="page page-dashboard ng-scope" ng-controller="DashboardCtrl">

            <div class="page-wrap">

                <!-- row -->
                <div class="row">

                    <!-- profile -->
                    <div class="col-md-12">
                        <div class="panel panel-default mb20 panel-hovered profile-widget">
                            <div class="panel-body">
                                <div class="clearfix mb15 top-info">
                                    <div class="left-side">
                                        <h3 class="text-light mt0">Robert Smith</h3>
                                        <p><strong>About:&nbsp;</strong>WebDesigner</p>
                                        <p><strong>Hobbies:&nbsp;</strong>Listening Music, learn new things and playing guitar.</p>
                                        <p>
                                            <strong>Skills: </strong>
                                            <label class="label label-pink">html5</label>
                                            <label class="label label-pink">css3</label>
                                            <label class="label label-pink">jquery</label>
                                        </p>
                                    </div>
                                    <div class="right-side">
                                        <img src="images/admin.jpg" alt="user">
                                        <div ng-init="x = 4" class="rating">
						   	 		<span ng-mouseleave="reset()" ng-keydown="onKeydown($event)" tabindex="0" role="slider" aria-valuemin="0" aria-valuemax="5" aria-valuenow="4" ng-model="x" max="5" state-on="'fa fa-star'" state-off="'fa fa-star-o'" class="text-warning ng-isolate-scope ng-valid" aria-invalid="false">
    <!-- ngRepeat: r in range track by $index --><i ng-repeat="r in range track by $index" ng-mouseenter="enter($index + 1)" ng-click="rate($index + 1)" class="glyphicon ng-scope fa fa-star" ng-class="$index < value &amp;&amp; (r.stateOn || 'glyphicon-star') || (r.stateOff || 'glyphicon-star-empty')" role="button" tabindex="0">
        <span class="sr-only ng-binding">(*)</span>
    </i><!-- end ngRepeat: r in range track by $index --><i ng-repeat="r in range track by $index" ng-mouseenter="enter($index + 1)" ng-click="rate($index + 1)" class="glyphicon ng-scope fa fa-star" ng-class="$index < value &amp;&amp; (r.stateOn || 'glyphicon-star') || (r.stateOff || 'glyphicon-star-empty')" role="button" tabindex="0">
        <span class="sr-only ng-binding">(*)</span>
    </i><!-- end ngRepeat: r in range track by $index --><i ng-repeat="r in range track by $index" ng-mouseenter="enter($index + 1)" ng-click="rate($index + 1)" class="glyphicon ng-scope fa fa-star" ng-class="$index < value &amp;&amp; (r.stateOn || 'glyphicon-star') || (r.stateOff || 'glyphicon-star-empty')" role="button" tabindex="0">
        <span class="sr-only ng-binding">(*)</span>
    </i><!-- end ngRepeat: r in range track by $index --><i ng-repeat="r in range track by $index" ng-mouseenter="enter($index + 1)" ng-click="rate($index + 1)" class="glyphicon ng-scope fa fa-star" ng-class="$index < value &amp;&amp; (r.stateOn || 'glyphicon-star') || (r.stateOff || 'glyphicon-star-empty')" role="button" tabindex="0">
        <span class="sr-only ng-binding">(*)</span>
    </i><!-- end ngRepeat: r in range track by $index --><i ng-repeat="r in range track by $index" ng-mouseenter="enter($index + 1)" ng-click="rate($index + 1)" class="glyphicon ng-scope fa fa-star-o" ng-class="$index < value &amp;&amp; (r.stateOn || 'glyphicon-star') || (r.stateOff || 'glyphicon-star-empty')" role="button" tabindex="0">
        <span class="sr-only ng-binding">( )</span>
    </i><!-- end ngRepeat: r in range track by $index -->
</span>
                                        </div>
                                    </div>
                                </div>
                                <ul class="user-badges list-unstyled row">
                                    <li class="col-xs-4">
                                        <i class="ion ion-ios-chatboxes-outline text-success"></i>
                                        <strong>192</strong>
                                        <button class="btn btn-success btn-xs mt15">View</button>
                                    </li>
                                    <li class="col-xs-4">
                                        <i class="ion ion-ios-heart-outline text-primary"></i>
                                        <strong>5K+</strong>
                                        <button class="btn btn-info btn-xs mt15">Follow</button>
                                    </li>
                                    <li class="col-xs-4">
                                        <i class="ion ion-ios-body text-danger"></i>
                                        <strong>32</strong>
                                        <button class="btn btn-primary btn-xs mt15">Profile</button>
                                    </li>
                                </ul>
                            </div> <!-- #end panel-body -->
                        </div>
                    </div>


                </div>


            </div> <!-- #end page-wrap -->
        </div></div>

</div> <!-- #end main-container -->
<?php include 'views/admin/layoutAdmin/footer.php';  ?>