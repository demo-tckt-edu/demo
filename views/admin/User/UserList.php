<?php  
include 'views/admin/layoutAdmin/head.php'; 
include 'views/admin/layoutAdmin/header.php'; 
?>
<!-- main-container -->
<div class="main-container clearfix">
  <!-- main-navigation -->
  <?php include 'views/admin/layoutAdmin/sidebar.php';  ?>
  <!-- #end main-navigation -->
  
  <div ng-class="{'nav-expand': navFull, 'fixedHeader': fixedHeader}" class="content-container ng-scope nav-expand fixedHeader" id="content" ng-view="">   <div class="page page-ui-extras ng-scope">

    <ol class="breadcrumb breadcrumb-small">
      <li>Home</li>
      <li class="active"><a href="#/tables/tables">Tables</a></li>
    </ol>


    <div class="page-wrap">

      <!-- row -->
      <div class="row">
        
        <!-- Basic Table -->
        <div class="col-md-12">
          <div class="panel panel-lined panel-hovered mb20 table-responsive">
            <div class="panel-body">
              <table class="table ng-scope" ng-controller="ResponsiveTableDemoCtrl" ng-class="{ 
              'table-bordered': tableModel.bordered, 
              'table-striped': tableModel.striped,
              'table-condensed': tableModel.condensed,
              'table-hover': tableModel.hover }">
              <thead>
                <tr>
                  <th class="col-lg-1"><button type="button" class="btn btn-default btn-sm fa fa-trash"></button></th>
                  <th>Id</th>
                  <th>UserName</th>
                  <th>Email</th>
                  <th>Name</th>
                  <th>Type</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
                  <?php
                    foreach($this->UserListModel as $key => $value) {
                      echo '<tr class="ng-scope">';
                      echo '<td>
                                    <div class="ui-checkbox ui-checkbox-primary ml5">
                                      <label><input type="checkbox"><span></span>
                                      </label>
                                    </div>
                              </td>';
                      echo "<td class=\"ng-binding\">{$value['id']}</td>";
                      echo "<td class=\"ng-binding\">{$value['username']}</td>";
                      echo "<td class=\"ng-binding\">{$value['email']}</td>";
                      echo "<td class=\"ng-binding\">{$value['name']}</td>";
                      echo "<td class=\"ng-binding\">{$value['type']}</td>";
                      echo "<td class=\"ng-binding\">{$value['status']}</td>";
                      echo '</tr>';

                    } ?>
              </tbody>
            </table>
            <div class="panel-footer clearfix">
              <p class="left mt15 small ng-binding">
                Showing 1 to 7 of 100 entries
              </p>
              <ul class="pagination-sm right pagination ng-isolate-scope ng-valid" boundary-links="true" total-items="filteredData.length" ng-model="currentPage" max-size="5" ng-change="select(currentPage)" items-per-page="numPerPage" rotate="false" previous-text="‹" next-text="›" first-text="«" last-text="»" tabindex="0" aria-invalid="false">
                <!-- ngIf: boundaryLinks --><li ng-if="boundaryLinks" ng-class="{disabled: noPrevious()}" class="ng-scope disabled"><a href="" ng-click="selectPage(1)" class="ng-binding" tabindex="0">«</a></li><!-- end ngIf: boundaryLinks -->
                <!-- ngIf: directionLinks --><li ng-if="directionLinks" ng-class="{disabled: noPrevious()}" class="ng-scope disabled"><a href="" ng-click="selectPage(page - 1)" class="ng-binding" tabindex="0">‹</a></li><!-- end ngIf: directionLinks -->
                <!-- ngRepeat: page in pages track by $index --><li ng-repeat="page in pages track by $index" ng-class="{active: page.active}" class="ng-scope active"><a href="" ng-click="selectPage(page.number)" class="ng-binding" tabindex="0">1</a></li><!-- end ngRepeat: page in pages track by $index --><li ng-repeat="page in pages track by $index" ng-class="{active: page.active}" class="ng-scope"><a href="" ng-click="selectPage(page.number)" class="ng-binding" tabindex="0">2</a></li><!-- end ngRepeat: page in pages track by $index --><li ng-repeat="page in pages track by $index" ng-class="{active: page.active}" class="ng-scope"><a href="" ng-click="selectPage(page.number)" class="ng-binding" tabindex="0">3</a></li><!-- end ngRepeat: page in pages track by $index --><li ng-repeat="page in pages track by $index" ng-class="{active: page.active}" class="ng-scope"><a href="" ng-click="selectPage(page.number)" class="ng-binding" tabindex="0">4</a></li><!-- end ngRepeat: page in pages track by $index --><li ng-repeat="page in pages track by $index" ng-class="{active: page.active}" class="ng-scope"><a href="" ng-click="selectPage(page.number)" class="ng-binding" tabindex="0">5</a></li><!-- end ngRepeat: page in pages track by $index --><li ng-repeat="page in pages track by $index" ng-class="{active: page.active}" class="ng-scope"><a href="" ng-click="selectPage(page.number)" class="ng-binding" tabindex="0">...</a></li><!-- end ngRepeat: page in pages track by $index -->
                <!-- ngIf: directionLinks --><li ng-if="directionLinks" ng-class="{disabled: noNext()}" class="ng-scope"><a href="" ng-click="selectPage(page + 1)" class="ng-binding" tabindex="0">›</a></li><!-- end ngIf: directionLinks -->
                <!-- ngIf: boundaryLinks --><li ng-if="boundaryLinks" ng-class="{disabled: noNext()}" class="ng-scope"><a href="" ng-click="selectPage(totalPages)" class="ng-binding" tabindex="0">»</a></li><!-- end ngIf: boundaryLinks -->
              </ul>
            </div>
          </div>
        </div>
      </div>

    </div>  
    
    
    <!-- #end row -->

  </div> <!-- #end page-wrap -->
</div></div>
</div> <!-- #end main-container -->
<?php include 'views/admin/layoutAdmin/footer.php';  ?>