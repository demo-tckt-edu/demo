  <?php  
  include 'views/admin/layoutAdmin/head.php'; 
  include 'views/admin/layoutAdmin/header.php'; 
  ?>
  <!-- slidebar #end header -->

  <!-- main-container -->
  <div class="main-container clearfix">
  	<!-- main-navigation -->
  	<?php include 'views/admin/layoutAdmin/sidebar.php';  ?>
  	<!-- #end main-navigation -->
  	
  	<!-- content-here -->
  	<div class="content-container nav-expand fixedHeader" id="content">
  		<div class="page page-forms-elements">

  			<ol class="breadcrumb breadcrumb-small">
  				<li>Users</li>
  				<li class="active"><a href="forms.elements.html">Add New</a></li>
  			</ol>

  			<div class="page-wrap">
  				<!-- row -->
  				<div class="row">
  					
  					<!-- col-left -->
  					<div class="col-sm-12 col-md-8">
  						<div class="panel panel-default panel-hovered panel-stacked mb30">
  							<div class="panel-heading">Add New User</div>
  							<div class="panel-body">
  								<form role="form" class="form-horizontal" action="javascript:;"> <!-- form horizontal acts as a row -->
  									<!-- normal control -->
  									<div class="form-group">
  										<label class="col-md-3 control-label">Username(required)</label>
  										<div class="col-md-9">
  											<input type="text" class="form-control">
  										</div>
  									</div>

  									<!-- with hint -->
  									<div class="form-group">
  										<label class="col-md-3 control-label">Email(required)</label>
  										<div class="col-md-9">
  											<input type="text" class="form-control" placeholder="Email ">
  											
  										</div>
  									</div>
  									
  									<!-- passowrd control -->
  									<div class="form-group">
  										<label class="col-md-3 control-label">Password</label>
  										<div class="col-md-9">
  											<input type="password" class="form-control" placeholder="Password">
  										</div>
  									</div>

  									
  									
  									
  									
  									<!-- success control -->
  									<div class="form-group">
  										<label class="col-md-3 control-label">First Name</label>
  										<div class="col-md-9">
  											<div class="has-success">
  												<input type="text" class="form-control">
  											</div>
  										</div>
  									</div>

  									<!-- warning control -->
  									<div class="form-group">
  										<label class="col-md-3 control-label">Last Name</label>
  										<div class="col-md-9">
  											<div class="has-warning">
  												<input type="text" class="form-control">
  											</div>
  										</div>
  									</div>
  									
  									<!-- error control -->
  									<div class="form-group">
  										<label class="col-md-3 control-label">Websites</label>
  										<div class="col-md-9">
  											<div class="has-error">
  												<input type="text" class="form-control">
  											</div>
  										</div>
  									</div>
                                    <div class="col-md-3">
                                        <label class="control-label">Image</label>

                                        <input type="file">

                                    </div>
  									<!-- native select -->
  									<div class="form-group">
  										<label class="col-md-3 control-label">Role</label>
  										<div class="col-md-9">
  											<select class="form-control">
  												<option value="0">Subscriber</option>
  												<option value="1">SEO Editor</option>
  												<option value="2">SEO Manager</option>
  												<option value="3">Shop Manager</option>
  												<option value="4">Author</option>
  												<option value="5">Editor</option>
  												<option value="6">Contributor</option>
  												<option value="7">Administrator</option>
  											</select>
  										</div>
  									</div>
  									<div class="clearfix right">
  										<button class="btn btn-primary mr5 waves-effect" type="submit">Save</button>
  										<button class="btn btn-default waves-effect">Cancel</button>
  									</div>
  								</form>
  							</div>
  						</div>
  					</div> <!-- #end col-left -->

  					




  					


  					

  				</div> <!-- #end page-wrap -->
  			</div> <!-- #end page -->
  		</div>
  		
  	</div> <!-- #end main-container -->
  	<?php include 'views/admin/layoutAdmin/footer.php';  ?>
