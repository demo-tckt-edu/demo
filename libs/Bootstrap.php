<?php

class Bootstrap {
	function __construct() 
	{
		$url = isset($_GET['url']) ? $_GET['url'] : null;
		$url = rtrim($url, '/');
		$url = explode('/', $url);
		//	print_r($url);die;
		if (empty($url[0])) {
			require 'controllers/home.php';
			$controller = new Home();
			$controller->index();
			return false;
		}
		// x? lý di?u hu?ng cho ph?n  admin 
		if (!empty($url[0])&&$url[0] =='admin') 
		{
			if (!empty($url[1])) 
			{    
				$path= $url[1];
	                 	// hàm có ch?c  nang ki?m tra xem url [1] có h?p lê hay không 
				$file='controllers/admin/'.$path.'.php';
	                      // ki?m tra xe url[1] có h?p l? hay không và url 2 có r?ng hay không n?u empty thi vô xét dk 
	                     // echo $file;
				    if(file_exists($file) && empty($url[2]))
	                       {   
	                       	require 'controllers/admin/'.$path.'.php';
	                       	$controller = new $path();
	                       	$controller->index();
	                       	return false;
	                       }
	                       else if(empty($url[2]))
	                       {   // echo " sang ";
	                           	$this->error();
	                         	return false;
	                       }
	                       $path2=$url[2];
	                       if(isset($path2))
	                       {   
	                           $file='controllers/admin/'.$path.'.php';
	                                if(!file_exists($file))
	                                {
                                         $this->error();
	                         	        return false;
	                                }
	                       	require 'controllers/admin/'.$path.'.php';
	                       	$controller = new $path;
	                       	$controller->loadModel($path);
	                       	if (isset($url[3])) 
	                       	{
	                       		if (method_exists($controller, $url[2])) 
	                       		{
	                       			$controller->{$url[2]}($url[3]);
	                       		}
	                       		else {
	                       			$this->error();	
	                       		}
	                       	} 
	                       	else 
	                       	{
	                       		if (isset($url[2])) 
	                       		{
	                       			if (method_exists($controller, $url[2])) 
	                       			{
	                       				
	                       				$controller->{$url[2]}();
	                       				return;
	                       			} else {
	                       				
	                       				$this->error();
	                       				return false;
	                       			}
	                       		} 
	                       		else {
	                       			$controller->index();
	                       		}
	                       	}
	                       }
	                   }
	                   else 
	                   {
	                   	require 'controllers/admin/Home.php';
	                   	$controller = new Home();
	                   	$controller->index();
	                   	return false;
	                   }
	               }
                   // x? lý di?u hu?ng cho ph?n giao di?n 
	               $file = 'controllers/' . $url[0] . '.php';
	               if (file_exists($file)) {
	               	require $file;
	               } else {
	               	$this->error();
	               	return false;
	               }
	               $controller = new $url[0];
	               $controller->loadModel($url[0]);
		       // calling methods
	               if (isset($url[2])) {
	               	if (method_exists($controller, $url[1])) {
				// echo " sang";
	               		$controller->{$url[1]}($url[2]);
	               	} else {
	               		$this->error();
	               	}
	               } else {
	               	if (isset($url[1])) {
	               		if (method_exists($controller, $url[1])) {
	               			$controller->{$url[1]}();
	               		} else {
	               			$this->error();
	               		}
	               	} else {
	               		$controller->index();
	               	}
	               }         
	           }	           
	           function error() {
	           	require 'controllers/error.php';
	           	$controller = new ControllerError();
	           	$controller->index();
	           	return false;
	           }
	       }